HELM IMAGE
----------

Image contains [HELM](https://helm.sh/) packager. Version of helm specified in Gitlab CI job

Example of image name:
```
algeran/helm:v3.0.2-alpine-3.10
```
