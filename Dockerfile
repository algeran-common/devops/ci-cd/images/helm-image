FROM alpine:3.10 as builder
ARG HELM_VERSION
RUN wget https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
        tar -zxvf helm-${HELM_VERSION}-linux-amd64.tar.gz

FROM gcr.io/distroless/base
COPY --from=builder /linux-amd64/helm /usr/local/bin/helm
